(function () {
  function install (Vue, options) {
    // default GitLab instance url, without trailing slash
    var url = 'https://gitlab.com'
    // GitLab user Private Token or Personal Access Token
    var token = 'you MUST provide an user Private Token or Personal Access Token'

    // reading options
    if (typeof options !== 'undefined') {
      // a token is mandatory to connect to GitLab API,
      // but it could be set later with .setToken
      if (typeof options.token !== 'undefined') {
        token = options.token
      }

      // an url may not be mandatory if user runs GitLab from localhost
      if (typeof options.url !== 'undefined') {
        // sanitizing url coming from options with no trailing slash
        url = options.url.replace(/\/$/, '')
      }
    }

    // GitLab API full url
    var apiUrl = url + '/api/v3'

    // adding an instance method by attaching it to Vue.prototype
    /**
     * A deadly simple Vue.js plugin to consume GitLab API.
     *
     * @mixin
     * @author {@link http://clorichel.com Pierre-Alexandre Clorichel}
     * @license {@link https://gitlab.com/clorichel/vue-gitlab-api/blob/master/LICENSE MIT License}
     * @see {@link https://gitlab.com/clorichel/vue-gitlab-api Project repository on GitLab.com}
     * @see {@link http://clorichel.com http://clorichel.com}
     */
    Vue.prototype.GitLabAPI = {
      /**
       * Set application wide GitLabAPI url value
       * @param {String} newUrl The new GitLab URL value
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.setUrl('https://your.gitlab-instance.com')
       *
       * // from a .vue component
       * this.GitLabAPI.setUrl('https://your.gitlab-instance.com')
       */
      setUrl: function (newUrl) {
        // sanitizing url with no trailing slash
        if (typeof newUrl === 'undefined') {
          console.warn('[vue-gitlab-api] This GitLab instance URL may not be correct')
          url = ''
        } else {
          url = newUrl.replace(/\/$/, '')
        }
        apiUrl = url + '/api/v3'
      },

      /**
       * Set application wide GitLabAPI token value
       * @param {String} newToken The new token value
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.setToken('your user token')
       *
       * // from a .vue component
       * this.GitLabAPI.setToken('your user token')
       */
      setToken: function (newToken) {
        if (typeof newToken === 'undefined' || newToken == null || newToken === '') {
          console.error('[vue-gitlab-api] You MUST provide a non empty Private Token or Personal Access Token')
          return
        }
        token = newToken
      },

      /**
       * A request callback function.
       *
       * @callback requestCallback
       * @param {Object} response Full response from GitLab API
       */

      /**
       * A request error callback function.
       *
       * @callback errorCallback
       * @param {Object} response Full response from GitLab API
       */

      /**
       * Issue a GET request on 'GitLabAPI_url/api/v3' with params and a variable to fill in
       * @param  {String}                  uri     The GitLab API uri to consume such as '/projects'
       * @param  {Object}                  params  A parameters object such as { 'project_id': 72 }
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitLab API, or a callback function fed with full response
       * @param  {errorCallback}           errorCb A callback function in case of error (response is passed to callback)
       * @example
       * // -------------------------------------------------
       * // 1- With an array to fill in a Vue.js defined data
       * // -------------------------------------------------
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.get('/projects', {}, [this.myGitLabData, 'projects'])
       * // from a .vue component
       * this.GitLabAPI.get('/projects', {}, [this.myGitLabData, 'projects'])
       *
       *
       * // ----------------------------------------------------------
       * // 2- With a callback function to play with the full response
       * // ----------------------------------------------------------
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.get('/projects', {}, (response) => {
       *   console.log('got response:', response)
       * })
       * // from a .vue component
       * this.GitLabAPI.get('/projects', {}, (response) => {
       *   console.log('got response:', response)
       * })
       */
      get: function (uri, params, fillIn, errorCb) {
        // verifying what user sends to fill in
        if (this._verifyFillIn(fillIn) !== true) {
          return
        }

        // ensure leading slash on uri
        uri = uri.replace(/^\/?/, '/')

        // downloading starts now
        this._updateStore('downloading')

        // request it with headers an params
        Vue.http.get(
          apiUrl + uri,
          {
            headers: {
              'PRIVATE-TOKEN': token
            },
            params: params
          }
        ).then((response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof fillIn === 'function') {
            // sending back the full response
            fillIn(response)
          } else {
            // fill in the data from the response body
            Vue.set(fillIn[0], fillIn[1], response.body)
          }
        }, (response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof errorCb === 'function') {
            // user defined an error callback function, calling it with response
            errorCb(response)
          } else {
            // no errorCb function defined, default to console error
            console.error('[vue-gitlab-api] GET ' + uri + ' failed: "' + response.body.message + '" on ' + apiUrl + ' (using token "' + token + '")')
          }
        })
      },

      /**
       * Issue a POST request on 'GitLabAPI_url/api/v3' with params and a variable to fill in
       * @param  {String}                  uri     The GitLab API uri to consume such as '/projects'
       * @param  {Object}                  params  A parameters object such as { 'project_id': 72 }
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitLab API, or a callback function fed with full response
       * @param  {errorCallback}           errorCb A callback function in case of error (response is passed to callback)
       * @example
       * // from anywhere in the global Vue scope
       * Vue.GitLabAPI.post('/projects/YOUR_PROJECT_ID/issues', {
       *   title: 'My new issues from vue-gitlab-api'
       * }, (response) => { console.log('posted it!', response) })
       *
       * // from a .vue component
       * this.GitLabAPI.post('/projects/YOUR_PROJECT_ID/issues', {
       *   title: 'My new issues from vue-gitlab-api'
       * }, (response) => { console.log('posted it!', response) })
       */
      post: function (uri, params, fillIn, errorCb) {
        // verifying what user sends to fill in
        if (this._verifyFillIn(fillIn) !== true) {
          return
        }

        // ensure leading slash on uri
        uri = uri.replace(/^\/?/, '/')

        // downloading starts now
        this._updateStore('downloading')

        // request it with headers an params
        Vue.http.post(
          apiUrl + uri,
          params,
          {
            headers: {
              'PRIVATE-TOKEN': token
            }
          }
        ).then((response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof fillIn === 'function') {
            // sending back the full response
            fillIn(response)
          } else {
            // fill in the data from the response body
            Vue.set(fillIn[0], fillIn[1], response.body)
          }
        }, (response) => {
          // no more downloading
          this._updateStore('downloaded')

          if (typeof errorCb === 'function') {
            // user defined an error callback function, calling it with response
            errorCb(response)
          } else {
            // no errorCb function defined, default to console error
            console.error('[vue-gitlab-api] POST ' + uri + ' failed: "' + response.body.message + '" on ' + apiUrl + ' (using token "' + token + '")')
          }
        })
      },

      /**
       * Register your application Vuex store to improve it with GitLabAPI Vuex store module
       * @param  {Object} store Your application Vuex store
       * @example
       * // from within a .vue component
       * this.GitLabAPI.registerStore(this.$store)
       */
      registerStore: function (store) {
        if (typeof store === 'undefined') {
          console.error('[vue-gitlab-api] This do not appear to be a Vuex store')
          return
        }

        // registering GitLabAPI Vuex module
        store.registerModule('GitLabAPI', {
          state: {
            // is GitLabAPI currently downloading?
            downloading: false,
            // how many downloads are running
            running: 0
          },
          mutations: {
            downloading: function (state) {
              // currently downloading
              state.running++
              state.downloading = true
            },
            downloaded: function (state) {
              // stopped downloading
              state.running--
              if (state.running === 0) {
                state.downloading = false
              }
            }
          }
        })

        // will need to update it later
        this._vuexStore = store
      },

      /**
       * If attached, the application wide Vuex store
       * @type {Object}
       * @private
       */
      _vuexStore: null,

      /**
       * Update the Vuex store if any
       * @param  {String} mutation The mutation to commit (downloading or downloaded)
       * @private
       */
      _updateStore: function (mutation) {
        if (this._vuexStore != null) {
          this._vuexStore.commit(mutation)
        }
      },

      /**
       * Verifying what to fill in from API consuming functions
       * @param  {(Array|requestCallback)} fillIn  The Vue.js defined data to fill in with results from GitLab API, or a callback function fed with full response
       * @return {Boolean}                         True if the fillIn type is ok, false (with a console error) if not
       * @private
       */
      _verifyFillIn: function (fillIn) {
        // fillIn can be a callback function to which the response will be sent
        if (typeof fillIn !== 'function') {
          // variable to fill in MUST be defined, as a Vue data (to be reactive),
          // and user MUST provide the key to fill in within that data, examples:
          // - [this.dataPropertyName, 'keyToFillDataIn']
          // - [this.GitLab, 'projects']
          if (!(fillIn instanceof Array) || fillIn.length < 2) {
            console.error('[vue-gitlab-api] You MUST define the Vue data you want to fill as a two values array')
            return false
          }

          // ensure reactive data is not an array but an object, or Vue.set will
          // fail in indexing the data to the expected key
          if (Array.isArray(fillIn[0]) || typeof fillIn[0] !== 'object') {
            console.error('[vue-gitlab-api] Your Vue data to fill MUST be an object (ie `{}`)')
            return false
          }
        }

        return true
      }
    }
  }

  if (typeof exports === 'object') {
    module.exports = install
  }
})()
