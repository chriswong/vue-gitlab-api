# Changelog
All notable changes to this project are documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/). For each version listed below, changes are grouped to describe their impact on the project, as follows:

- `Added` for new features
- `Changed` for changes in existing functionality
- `Deprecated` for once-stable features removed in upcoming releases
- `Removed` for deprecated features removed in this release
- `Fixed` for any bug fixes
- `Security` to invite users to upgrade in case of vulnerabilities

## [Unreleased]
### Added
- Now able to use a callback function with full response for GET requests, or to assign data as usual
- Support for POST requests on GitLab API with custom parameters
- Ability fo use a callback function with full response for POST requests, or to assign data returned to Vue.js component data
- Made it clear using Private Token or Personal Access Token is the same
- work in progress on [clorichel/vue-gitlab-api issues](https://gitlab.com/clorichel/vue-gitlab-api/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=Feature)...

## 0.1.7 - 2016-10-29
### Added
- Now able to use a callback function with full response for GET requests, or to assign data as usual
- Support for POST requests on GitLab API with custom parameters
- Ability fo use a callback function with full response for POST requests, or to assign data returned to Vue.js component data
- Made it clear using Private Token or Personal Access Token is the same

## 0.1.6 - 2016-10-26
### Added
- Vuex store module provides downloading state (Boolean) and total number or running requests
- Using Vuex? You can now register your application Vuex store to improve it with GitLabAPI Vuex store module

## 0.1.5 - 2016-10-26
### Added
- Improved Readme and example with changed behavior
- Filling in the reactive data property now uses stock Vue.set
- Error messages on API request is now much readable

### Changed
- Reactive data to fill in do no more have to be an empty array, but an empty object

## 0.1.4 - 2016-10-25
### Added
- One can now send in a callback function to customize behavior on requests errors

## 0.1.3 - 2016-10-25
### Fixed
- Library dist file is now built to make it usable

## 0.1.2 - 2016-10-25
### Added
- Readme now gives credit for the chosen logo

### Fixed
- Ensured that dist folder is deployed with npm package

## 0.1.1 - 2016-10-25
### Added
- Just bumped version to npm publish again

## 0.1.0 - 2016-10-25
### Added
- This CHANGELOG file to hopefully serve as an evolving example of a standardized open source project CHANGELOG
- README describes main topics to get started
- Now published as the vue-gitlab-api NPM package to simplify installing and using
- Support for GET requests on GitLab API with custom parameters, assigning data returned to Vue.js component data
- Library supports setting GitLab instance URL and user Private Token after booting
- GitLabAPI.js has been created as a Vue.js plugin
- A DevTest vue has been created for developers convenience
- Initial vue-cli scaffolding on webpack template with unit tests

[Unreleased]: https://gitlab.com/clorichel/vue-gitlab-api/compare/v0.1.7...master